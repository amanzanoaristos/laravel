<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CochesController;
use App\Http\Controllers\ConcesionarioController;
use App\Http\Controllers\UsuariosController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/logout', function () {
    return view('dashboard');
});
Route::get('/concesionario', function () {
    return view('concesionario.index');
});
Route::get('/usuarios', function () {
    return view('usuarios.index');
});
Route::get('/dashboard', [CochesController::class, 'index'])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
//Route::get('/home',[Cochescontroller::class, 'index'])->middleware('auth')->name('indexCoche');
Route::get('/coches' , [CochesController::class, 'index'])->middleware('auth')->name('coche.index');
Route::get('/coches/create' , [CochesController::class, 'create'])->name('coche.create');
Route::post('/coches/store' , [CochesController::class, 'store'])->name('coche.store');
Route::get('/coches/edit/{id}' , [CochesController::class, 'edit'])->name('coche.edit');
Route::post('/coches/update/{id}' , [CochesController::class, 'update'])->name('coche.update');
Route::any('/coches/destroy/{id}' , [CochesController::class, 'destroy'])->name('coche.destroy');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');



Route::get('/concesionario',[Concesionariocontroller::class, 'index'])->middleware('auth')->name('indexConcesionario');
//Route::get('/concesionario' , [ConcesionarioController::class, 'index'])->name('coche.index');
Route::get('/concesionario/create' , [ConcesionarioController::class, 'create'])->name('concesionario.create');
Route::post('/concesionario/store' , [ConcesionarioController::class, 'store'])->name('concesionario.store');
Route::get('/concesionario/edit/{id}' , [ConcesionarioController::class, 'edit'])->name('concesionario.edit');
Route::post('/concesionario/update/{id}' , [ConcesionarioController::class, 'update'])->name('concesionario.update');
Route::any('/concesionario/destroy/{id}' , [ConcesionarioController::class, 'destroy'])->name('concesionario.destroy');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout'); 



Route::get('/usuarios',[UsuariosController::class, 'index'])->middleware('auth')->name('indexusuarios');
//Route::get('/usuarios' , [usuariosController::class, 'index'])->name('coche.index');
Route::get('/usuarios/create' , [UsuariosController::class, 'create'])->name('usuarios.create');
Route::post('/usuarios/store' , [UsuariosController::class, 'store'])->name('usuarios.store');
Route::get('/usuarios/edit/{id}' , [UsuariosController::class, 'edit'])->name('usuarios.edit');
Route::post('/usuarios/update/{id}' , [UsuariosController::class, 'update'])->name('usuarios.update');
Route::any('/usuarios/destroy/{id}' , [UsuariosController::class, 'destroy'])->name('usuarios.destroy');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout'); 


Route::resource('projects', 'ConcesionarioController');
Route::resource('projects', 'CochesController');