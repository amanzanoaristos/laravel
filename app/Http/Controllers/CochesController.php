<?php

namespace App\Http\Controllers;

use App\Models\Coches;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class CochesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['coches']=Coches::paginate(5);

        return view('coches.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datosdecoches= request()->all();
        $datosdecoches= request()->except('_token');

        if($request->hasFile('foto')){

            $datosdecoches['foto']=$request->file('foto')->store('uploads','public');

        }

        Coches::insert($datosdecoches);

       // return response()->json($datosdecoches);
       return redirect('coches')->with('mensaje','coche introducido correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coches  $coches
     * @return \Illuminate\Http\Response
     */
    public function show(Coches $coches)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coches  $coches
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $coches=Coches::findOrFail($id);
        return view('coches.edit',compact('coches'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coches  $coches
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $datosdecoches= request()->except(['_token', '_method']);

        if($request->hasFile('foto')){
            $coches=Coches::findOrFail($id);
            Storage::delete('public/'.$coches->foto);
            
            $datosdecoches['foto']=$request->file('foto')->store('uploads','public');

        }



        Coches::where('id','=',$id)->update($datosdecoches);
        $coches=Coches::findOrFail($id);
        return redirect('coches')->with('mensaje','coche editado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coches  $coches
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $coches=Coches::findOrFail($id);

        if(Storage::delete('public/'.$coches->foto)){


            coches::destroy($id);
        
        }
        return redirect('coches')->with('mensaje','coche eliminado correctamente');


    }
    
}
