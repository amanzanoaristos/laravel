<?php

namespace App\Http\Controllers;

use App\Models\concesionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class concesionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['concesionario']=concesionario::paginate(5);

        return view('concesionario.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('concesionario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datosdeconcesionario= request()->all();
        $datosdeconcesionario= request()->except('_token');

        if($request->hasFile('foto')){

            $datosdeconcesionario['foto']=$request->file('foto')->store('uploads','public');

        }

        concesionario::insert($datosdeconcesionario);

       // return response()->json($datosdeconcesionario);
       return redirect('concesionario')->with('mensaje','concesionario introducido correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\concesionario  $concesionario
     * @return \Illuminate\Http\Response
     */
    public function show(concesionario $concesionario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\concesionario  $concesionario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $concesionario=concesionario::findOrFail($id);
        return view('concesionario.edit',compact('concesionario'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\concesionario  $concesionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $datosdeconcesionario= request()->except(['_token', '_method']);

        if($request->hasFile('foto')){
            $concesionario=concesionario::findOrFail($id);
            Storage::delete('public/'.$concesionario->foto);
            
            $datosdeconcesionario['foto']=$request->file('foto')->store('uploads','public');

        }



        Concesionario::where('id','=',$id)->update($datosdeconcesionario);
        $concesionario=Concesionario::findOrFail($id);
        return redirect('concesionario')->with('mensaje','concesionario editado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\concesionario  $concesionario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $concesionario=Concesionario::findOrFail($id);

        if(Storage::delete('public/'.$concesionario->foto)){


            Concesionario::destroy($id);
        
        }
        return redirect('concesionario')->with('mensaje','concesionario eliminado correctamente');


    }
    
}