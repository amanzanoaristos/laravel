<?php

namespace App\Http\Controllers;

use App\Models\usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class usuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['usuarios']=usuarios::paginate(5);

        return view('usuarios.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datosdeusuarios= request()->all();
        $datosdeusuarios= request()->except('_token');

        if($request->hasFile('foto')){

            $datosdeusuarios['foto']=$request->file('foto')->store('uploads','public');

        }

        usuarios::insert($datosdeusuarios);

       // return response()->json($datosdeusuarios);
       return redirect('usuarios')->with('mensaje','usuario introducido correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuarios=usuarios::findOrFail($id);
        return view('usuarios.edit',compact('usuarios'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $datosdeusuarios= request()->except(['_token', '_method']);

        if($request->hasFile('foto')){
            $usuarios=usuarios::findOrFail($id);
            Storage::delete('public/'.$usuarios->foto);
            
            $datosdeusuarios['foto']=$request->file('foto')->store('uploads','public');

        }



        usuarios::where('id','=',$id)->update($datosdeusuarios);
        $usuarios=usuarios::findOrFail($id);
        return redirect('usuarios')->with('mensaje','coche editado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuarios=usuarios::findOrFail($id);

        if(Storage::delete('public/'.$usuarios->foto)){


            usuarios::destroy($id);
        
        }
        return redirect('usuarios')->with('mensaje','coche eliminado correctamente');


    }
    
}
