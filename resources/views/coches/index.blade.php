
<link rel="stylesheet" type="text/css" href="{{asset('datatable/css/dataTables.bootstrap4.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('datatable/css/bootstrap.css')}}"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>





<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{route('coche.index')}}">Coches</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('indexConcesionario')}}">Conce</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="{{route('indexusuarios')}}"> Usuarios </a>
        </li>
        <li class="nav-item">
            
          <form method="POST" action="{{ route('logout') }}">
          <p><button type="submit">logout</button></p>

                            @csrf
            </form>
            
        </li>
      </ul>
    </div>
  </div>
</nav>
 @if(Session::has('mensaje'))
 {{Session::get('mensaje')}}
 @endif
<div class="card">
    <div class="card-header">
        <a href="{{route('coche.create')}}" class="btn btn-primary">Registrar nuevo coche</a>
    </div>
    <div class="card-body">
        
                <table id="tabla" class="table table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Descripcio</th>
                            <th>Contacto</th>
                            <th>Foto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($coches as $c)

                        <tr>

                            <td>{{$c->id}}</td>
                            <td>{{$c->marca}}</td>
                            <td>{{$c->modelo}}</td>
                            <td>{{$c->descripcion}}</td>
                            <td>{{$c->contacto}}</td>
                            <td>
                            <img src="{{asset('storage').'/'.$c->foto   }}" alt="10">
                            {{$c->foto}}</td>
                            <td>
                            
                            <a href="{{ route ('coche.edit', $c->id) }}">
                            Editar  | 
                                </a>

                                <form action="{{ route ('coche.destroy', $c->id) }}" method="post">
                                
                                @csrf 
                                {{ method_field('DELETE') }}
                                <input type="submit" onclick="return confirm('quieres borrar?')" value="borrar">
                                
                                </form>

                                </td>
                        </tr>
                        @endforeach
                        </tbody>
                    <tfooter>

                        <tr>
                            <th>#</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Descripcio</th>
                            <th>Contacto</th>
                            <th>Foto</th>
                            <th>Acciones</th>
                        </tr>
                    </tfooter>
                </table>
            </div>
           
        </div>  
    </div>
  
</div>










    <script type="text/javascript" src="{{asset('datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('datatable/js/jquery-3.5.1.js')}}"></script>
    <script type="text/javascript" src="{{asset('datatable/js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#tabla').DataTable();
        } );
    </script>


