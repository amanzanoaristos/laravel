

<form action="{{ route ('usuarios.update', $usuarios->id)}}" method="post" enctype="multipart/form-data" >
@csrf

@include('usuarios.form',['modo'=>'editar']);
</form>